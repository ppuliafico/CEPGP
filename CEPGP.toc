## Interface:11200
## Title: Classic EPGP
## Author: Originally by Alumian.  Updated for <Glorious> by Carbon
## Notes: CEPGP handles your guild's EPGP and master loot distribution. Usage: /cepgp for options
## SavedVariables: CHANNEL, MOD, AUTOEP, EPVALS, BASEGP, STANDBYEP, STANDBYOFFLINE, STANDBYPERCENT, STANDBYRANKS, SLOTWEIGHTS, RECORDS, OVERRIDE_INDEX, TRAFFIC

CEPGP_frame.xml
CEPGP.lua
CEPGP_index.lua
